jQuery(document).ready(function($) {
  $("#nav ul.menu-parent").superfish({ 
          delay:       100,                           
          animation:   {opacity:'show',height:'show'},  
          speed:       'fast',                          
          autoArrows:  true,                           
          dropShadows: true                   
      });
});

const showToggleMenu = document.querySelector("#menu");
const menuToggleBtn = document.querySelector(".menu-toggle-btn");
menuToggleBtn.addEventListener("click", function() {
  menuToggleBtn.classList.toggle("js-transform");
  showToggleMenu.classList.toggle("show-menu");
});
